const assert            = require('assert');
const http              = require ('http');
const map               = require('lodash');
const Promise           = require('bluebird');
const mongoose          = require('mongoose');
const moment            = require('moment');
const server            = require ('./../app');

const indexRoute        = require('./../routes/user');
const userRoute         = require('./../routes/user');
const restaurantRoute   = require('./../routes/restaurant');
const voteRoute         = require('./../routes/vote');

const UserCollection  = 'restaurant-users';
const RestaurantCollection  = 'restaurant-restaurants';
const VoteCollection  = 'restaurant-votes';

const getCollectionNames = function() {
    return new Promise(function(resolve, reject) {
        mongoose.connection.db.listCollections().toArray(function(err, collections) {
            if (err) {
                return reject(err);
            }
            resolve(collections);
        });
    });
};

const getTimeToVote = function(){
    let currentTime= moment('12:00 pm', "HH:mm a");
    let startTime = moment('00:00 am', "HH:mm a");
    let endTime = moment('11:59 am', "HH:mm a");
    let amIBetween = currentTime.isBetween(startTime , endTime);

    return amIBetween;      
};

describe('Get Route', function(){
    describe('Index Route', function(){
        it('- should get Index Route', function (done) {
            http.get(indexRoute, function (response) {
                assert.equal(response.statusCode, 200);
                done();
            });
        });
    });
    describe('User Route', function(){
        it('- should get User Route', function(done){
            http.get(userRoute, function(response){
                assert.equal(response.statusCode, 200);
                done();
            });
        });
    });
    describe('Restaurant Route', function(){
        it('- should get Restaurant Route', function(done){
            http.get(restaurantRoute, function(response){
                assert.equal(response.statusCode, 200);
                done();
            });
        });
    });
    describe('Vote Route', function(){
        it('- should get Vote Route', function(done){
            http.get(voteRoute, function(response){
                assert.equal(response.statusCode, 200);
                done();
            });
        });
    });
});

describe('Get Collection', function() {
    describe('Users', function(){
        it('- should get Collection Users', function(done){
            getCollectionNames().then(function(names){
                const userCollection = names.filter(function(item) { 
                    return item.name === UserCollection; 
                });
                assert.equal(userCollection[0].name.indexOf(UserCollection), 0);
            })
            .then(function() {
                done();
            }).catch(done);
        });
    });
    describe('Restaurants', function(){
        it('- should get Collection Restaurants', function(done){
            getCollectionNames().then(function(names){
                const restaurantCollection = names.filter(function(item) { 
                    return item.name === RestaurantCollection; 
                });
                assert.equal(restaurantCollection[0].name.indexOf(RestaurantCollection), 0);
            })
            .then(function() {
                done();
            }).catch(done);
        });
    });   
    describe('Votes', function(){
        it('- should get Collection Votes', function(done){
            getCollectionNames().then(function(names){
                const voteCollection = names.filter(function(item) { 
                    return item.name === VoteCollection; 
                });
                assert.equal(voteCollection[0].name.indexOf(VoteCollection), 0);
            })
            .then(function() {
                done();
            }).catch(done);
        });
    });     
});

describe('Check Dates and time', function(){
    describe('Vote time', function(){
        const time = getTimeToVote();
        it('- should not allow to vote after noon', function(done){
            assert.notEqual(time, true);
            done();
        });
    });
});