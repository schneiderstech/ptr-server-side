const RestaurantSchema = require('../models/restaurant');

// list all restaurants
module.exports.listAllRestaurants = function(req, res) {
    RestaurantSchema
    .find().sort({_id: 'desc'}).find({}, function(err, listallrestaurants){
        if(err){
          console.log(err);
        } else {
            res.json(listallrestaurants);
        }
    });
};

module.exports.addNewRestaurant = function(req, res){
    let newRestaurant = new RestaurantSchema();
    newRestaurant.description = req.body.description;

    // save the restaurant
    newRestaurant.save(function (err, done) {
        if (err){
            return res.json(err);
        }
        return res.json(done);
    });
};

