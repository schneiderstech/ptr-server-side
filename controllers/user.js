const UserSchema = require('../models/user');
// list user by ID
module.exports.getById = function(req, res) {
    UserSchema
    .findById({ _id : req.params.id }, function(err, user){
        if(err){
          console.log(err);
        } else {
            res.json(user);
        }
    });
};

module.exports.listAllUsers = function(req, res) {
    UserSchema
    .find().sort({_id: 'desc'}).find({}, function(err, listallusers){
        if(err){
          console.log(err);
        } else {
            res.json(listallusers);
        }
    });
};

module.exports.addNewUser = function(req, res){
    let newUser = new UserSchema();
    newUser.name = req.body.name;

    // save the user
    newUser.save(function (err, done) {
        if (err){
            return res.json(err);
        }
        return res.json(done);
    });
};
