const VoteSchema        = require('../models/vote');
const moment            = require('moment');
// list all votes of the week
module.exports.listAllVotesByWeek = function(req, res) {
    let starEndWeek = weekStartEnd();
    VoteSchema
    .find({
        dateVoted:{
            $gte:starEndWeek[0], 
            $lte:starEndWeek[1]
        }
    }, function(err, listallbyweek){
        if(err){
          console.log(err);
        } else {
            res.json(listallbyweek);
        }
    });

    function weekStartEnd(){        
        let currentDate = moment();
        let weekStart = currentDate.clone().startOf('week').format();
        let weekEnd = currentDate.clone().endOf('week').format();
        let days = [weekStart, weekEnd];
        
        return days;
    };    
};

module.exports.addNewVote = function(req, res){
    let newVote = new VoteSchema();
    newVote.userId = req.body.user._id;
    newVote.userName = req.body.user.name;
    newVote.restaurantId = req.body.restaurant._id;
    newVote.restaurantDescription = req.body.restaurant.description;    
    newVote.dateVoted = req.body.dateVoted;

    checkVotePerUserPerDay(newVote);

    function checkVotePerUserPerDay(newVote){
        VoteSchema
        .find({$and: [{userId: newVote.userId }, {dateVoted: newVote.dateVoted } ]}, function(err, exist){
            if(exist.length > 0){
                res.json('existInDay');
            } else {
                checkVotePerWeek(newVote);
            }
        });
    };

    function checkVotePerWeek(newVote){
        let starEndWeek = weekDay();
        VoteSchema
        .find({$and:[
            {
                userId: newVote.userId 
            }, 
            {
                restaurantId: newVote.restaurantId 
            }, 
            {
                dateVoted: {$gte:starEndWeek[0], $lte:starEndWeek[1]}, 
            }                          
            ]}, function(err, exist){
            if(exist.length > 0){
                console.log('exist: ', exist);
                res.json('existInWeek');
            } else {
               saveVote(newVote);
            }
        });
    };

    function saveVote(newVote){
        newVote.save(function (err, done) {
            if (err){
                return res.json(err);
            }
            return res.json(done);
        });
    };

    function weekDay(){        
        let dateVoted = moment(newVote.dateVoted);
        let weekStart = dateVoted.clone().startOf('week').format();
        let weekEnd = dateVoted.clone().endOf('week').format();
        let days = [weekStart, weekEnd];
        
        return days;
    };    
};
