const express = require('express');
const router  = express.Router();
const bodyParser = require('body-parser');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

let VoteController = require('./../controllers/vote');

router.get('/get-all-votes-by-week', VoteController.listAllVotesByWeek);
router.post('/add-new-vote', VoteController.addNewVote);

module.exports = router;