const express = require('express');
const router  = express.Router();
const bodyParser = require('body-parser');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

let RestaurantController = require('./../controllers/restaurant');

router.get('/get-all-restaurants', RestaurantController.listAllRestaurants);
router.post('/add-new-restaurant', RestaurantController.addNewRestaurant);

module.exports = router;