const express = require('express');
const router  = express.Router();
const bodyParser = require('body-parser');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

let UserController = require('./../controllers/user');

router.get('/get-all-users', UserController.listAllUsers);
router.get('/get-user/:id', UserController.getById);
router.post('/add-new-user', UserController.addNewUser);

module.exports = router;