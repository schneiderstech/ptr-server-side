const mongoose = require('mongoose');

let Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

let VoteSchema = new mongoose.Schema({
    restaurantId: {
        type: ObjectId, ref: 'restaurant'
    },
    restaurantDescription: {
        type: String
    },
    userId: {
        type: ObjectId, ref: 'user'
    },
    userName: {
        type: String
    },
    dateVoted: {
        type : Date
    }
}, {
    timestamps: true
});

let Model = mongoose.model('restaurant-votes', VoteSchema);

module.exports = Model;
