const mongoose = require('mongoose');

let UserSchema = new mongoose.Schema({
    name: {
        type: String,
    }
});

let Model = mongoose.model('restaurant-users', UserSchema);

module.exports = Model;
