const mongoose = require('mongoose');

let RestaurantSchema = new mongoose.Schema({
    description: {
        type: String,
    }
});

let Model = mongoose.model('restaurant-restaurants', RestaurantSchema);

module.exports = Model;
