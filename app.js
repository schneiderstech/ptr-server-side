const express     = require('express');
const path        = require('path');
const bodyParser  = require('body-parser');
const favicon     = require('serve-favicon');
const databaseConfig = require('./config/database');
const mongoose    = require('mongoose');

const index       = require('./routes/index');
const user        = require('./routes/user');
const restaurant  = require('./routes/restaurant');
const vote        = require('./routes/vote');

let port = 8080;
let app = express();


// CONNECTION EVENTS
mongoose.connect(databaseConfig.url, { useMongoClient: true });

// When successfully connected
mongoose.connection.on('connected', function () {  
  console.log('Mongoose default connection open to ' + databaseConfig.url);
}); 

// If the connection throws an error
mongoose.connection.on('error',function (err) {  
  console.log('Mongoose default connection error: ' + err);
}); 

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {  
  console.log('Mongoose default connection disconnected'); 
});

// View Engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

//Set static folder
// add static files from client-side directory
app.use(express.static(path.join(__dirname, '..', '/ptr-client-side/src')));
app.use(express.static(path.join(__dirname, '..', '/ptr-client-side/node_modules')));

// Body Parser 
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(favicon(path.join(__dirname, '..', '/ptr-client-side/src/assets/favicon.ico')));

app.use('/', index);
app.use('/', user);
app.use('/', restaurant);
app.use('/', vote);

app.listen(port, function() {
    console.log('Server started on port ' + port);
});
